from core.config import WEBHOOK_PATH, WEBHOOK_URI, BOT_TOKEN
from core.loader import bot, dp
from api import app
import json
import logging.config
import logging

async def setup_logging():
    with open('logs/logger_config.json', 'r') as f:
        config = json.load(f)
        logging.config.dictConfig(config)

async def set_webhook():
    await bot.set_webhook(WEBHOOK_URI)


async def on_startup():
    bot_logger = logging.getLogger('bot_logger')
    bot_logger.info("Bot started")
    await setup_logging()
    await set_webhook()



if __name__ == "__main__":
    bot_logger = logging.getLogger('bot_logger')
    try:
        app.add_event_handler("startup", on_startup)

        import uvicorn
        uvicorn.run(app, host="127.0.0.1", port=7280, log_level="info", log_config="logs/logger_config.json")
    except KeyboardInterrupt:
        pass
    finally:
        bot_logger.info("Bot stopped")