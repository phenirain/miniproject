from typing import AsyncGenerator

from sqlalchemy import BigInteger, Column, String, Integer, TIMESTAMP, ForeignKey, REAL
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

from core.config import DB_HOST, DB_NAME, DB_PASS, DB_PORT, DB_USER
from models.models import user, task

DATABASE_URL = f"postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
Base: DeclarativeMeta = declarative_base()


class User(Base):
    __tablename__ = 'user'

    user_id = Column(BigInteger, primary_key=True, index=True)
    balance = Column(REAL, default=0.0)
    referal_id = Column(BigInteger)


class Task(Base):
    __tablename__ = 'task'

    task_id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    title = Column(String)
    url = Column(String, unique=True)
    award = Column(REAL)
    task_added_at = Column(TIMESTAMP, default=func.now())
    task_ended_at = Column(TIMESTAMP)


class CompletedTask(Base):
    __tablename__ = 'completed_task'

    task_id = Column(Integer, ForeignKey(task.c.task_id), primary_key=True)
    user_id = Column(BigInteger, ForeignKey(user.c.user_id), primary_key=True)


engine = create_async_engine(DATABASE_URL)
async_session_maker = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)

async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


