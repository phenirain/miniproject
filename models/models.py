from sqlalchemy import MetaData, BigInteger, Integer, String, ForeignKey, Table, Column, TIMESTAMP, REAL


metadata = MetaData()


user = Table(
    'user', metadata,
    Column('user_id', BigInteger, primary_key=True, index=True),
    Column('balance', REAL),
    Column('referal_id', BigInteger)
)

task = Table(
    'task', metadata,
    Column('task_id', Integer, primary_key=True, index=True, autoincrement=True),
    Column('title', String),
    Column('url', String, unique=True),
    Column('award', REAL),
    Column('task_added_at', TIMESTAMP),
    Column('task_ended_at', TIMESTAMP),
)

completed_task = Table(
    'completed_task', metadata,
    Column('task_id', Integer, ForeignKey(task.c.task_id), primary_key=True),
    Column('user_id', BigInteger, ForeignKey(user.c.user_id), primary_key=True)
)


