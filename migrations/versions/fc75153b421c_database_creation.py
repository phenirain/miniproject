"""Database creation

Revision ID: fc75153b421c
Revises: 
Create Date: 2024-06-29 23:15:52.485154

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'fc75153b421c'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('task',
        sa.Column('task_id', sa.Integer(), nullable=False),
        sa.Column('url', sa.String(), nullable=True),
        sa.Column('award', sa.BigInteger(), nullable=True),
        sa.Column('task_added_at', sa.TIMESTAMP(), nullable=True),
        sa.Column('task_ended_at', sa.TIMESTAMP(), nullable=True),
        sa.PrimaryKeyConstraint('task_id'),
        sa.UniqueConstraint('url')
    )
    op.create_index(op.f('ix_task_task_id'), 'task', ['task_id'], unique=False)
    op.create_table('user',
        sa.Column('user_id', sa.BigInteger(), nullable=False),
        sa.Column('balance', sa.BigInteger(), nullable=True),
        sa.Column('referal_id', sa.BigInteger(), nullable=True),
        sa.PrimaryKeyConstraint('user_id')
    )
    op.create_index(op.f('ix_user_user_id'), 'user', ['user_id'], unique=False)
    op.create_table('completed_task',
        sa.Column('task_id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.BigInteger(), nullable=False),
        sa.ForeignKeyConstraint(['task_id'], ['task.task_id'], ),
        sa.ForeignKeyConstraint(['user_id'], ['user.user_id'], ),
        sa.PrimaryKeyConstraint('task_id', 'user_id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('completed_task')
    op.drop_index(op.f('ix_user_user_id'), table_name='user')
    op.drop_table('user')
    op.drop_index(op.f('ix_task_task_id'), table_name='task')
    op.drop_table('task')
    # ### end Alembic commands ###
