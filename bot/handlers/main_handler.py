from aiogram.filters import CommandStart, Command, StateFilter
from aiogram.utils.deep_linking import create_start_link
from aiogram.types import Message
from aiogram.fsm.context import FSMContext
from aiogram import Router, F, Bot
from bot import log_decorator
from core.config import BOT_LOGGER
from typing import Optional
import json
import aiohttp

main_router = Router()


async def post_data(url, data):
    try:
        headers = {
            'Content-Type': 'application/json'
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(url, data=json.dumps(data), headers=headers) as response:
                return await response.json()
    except Exception as e:
        print(e)

async def register_user(user_id: int, referal_id: Optional[int] = None):
    data = {
        "user_id": user_id,
        "balance": 500,
        "referal_id": referal_id
    }
    res = await post_data("http://localhost:8080/api/v1/user", data=data)
    await res

@main_router.message(StateFilter('*'), CommandStart())
@log_decorator(BOT_LOGGER)
async def start(message: Message, state: FSMContext):
    try:
        await message.answer("Добро пожаловать!")
        find_referal: str = message.text.split(' ')[-1]
        referal_id = None
        if find_referal != '/start':
            referal_id: int = int(find_referal)
        await register_user(user_id=message.chat.id, referal_id=referal_id)
    except ValueError:
        pass


