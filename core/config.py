from os import getenv
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(filename="../.env"))

BOT_TOKEN = getenv('BOT_TOKEN')
BOT_LOGGER = getenv('BOT_LOGGER')
WEBHOOK_TOKEN = getenv('WEBHOOK_TOKEN')
WEBHOOK_PATH = getenv('BOT_TOKEN')
BASE_WEBHOOK_URL = getenv('BASE_WEBHOOK_URL')
WEBHOOK_URI = BASE_WEBHOOK_URL + '/' + WEBHOOK_PATH
DB_HOST = getenv("DB_HOST")
DB_PORT = getenv("DB_PORT")
DB_NAME = getenv("DB_NAME")
DB_USER = getenv("DB_USER")
DB_PASS = getenv("DB_PASS")







