from api.repositories import CompletedTaskRepository
from api.services.TaskService import TaskService
from api.services.UserService import UserService
from api.schemas import SCompletedTask, SUserPut
from db import CompletedTask, Task
from core.loader import bot
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Optional, Tuple
from aiogram.types.chat_member import ChatMember
from aiogram.types.chat_member_administrator import ChatMemberAdministrator
from aiogram.types.chat_member_owner import ChatMemberOwner



class CompletedService():
    def __init__(self, db_session: AsyncSession) -> None:
        self.compl_repo = CompletedTaskRepository(db_session)
        self.task_service = TaskService(db_session)
        self.user_service = UserService(db_session)

    async def get_uncompleted_tasks_by_user_id(self, user_id: int) -> Optional[Tuple[Task]]:
        return await self.compl_repo.get_uncompleted_tasks_by_user_id(user_id)
    
    async def __add_completed_task(self, completed_task: CompletedTask, task: Task):
        user_id = completed_task.user_id
        user: SUserPut = SUserPut(user_id=user_id, balance=task.award)
        await self.user_service.change_balance(user)
        await self.compl_repo.add_completed_task(completed_task)

    async def check_task(self, completed_task: SCompletedTask) -> Tuple[bool, Optional[CompletedTask]]:
        completed_task = CompletedTask(**completed_task.model_dump())
        find_completed = await self.compl_repo.get_completed_task_by_task_id(completed_task.task_id)
        if find_completed:
            return False, None
        task: Task = await self.task_service.get_task_by_id(completed_task.task_id)
        index: int = task.url.rfind('t.me/')
        chat_name: str = f"@{task.url[index+5:]}" 
        result = await bot.get_chat_member(chat_id=chat_name, user_id=completed_task.user_id)
        if isinstance(result, ChatMemberOwner) or isinstance(result, ChatMember) or isinstance(result, ChatMemberAdministrator):
            await self.__add_completed_task(completed_task, task)
            return True, completed_task
        else:
            return False, None

    

