from api.repositories import UserRepository
from api.schemas import SUser, SUserPut
from db import User
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Tuple, Optional


class UserService():
    def __init__(self, db_session: AsyncSession) -> None:
        self.user_repo = UserRepository(db_session)

    async def exist(self, user: SUser):
        user: User = User(**user.model_dump())
        res = await self.user_repo.get_user_by_id(user_id=User.user_id)
        if res:
            return True
        else:
            return False


    async def register_user(self, user: SUser):
        new_user: User = User(**user.model_dump())
        return await self.user_repo.add_user(new_user)
    

    async def change_balance(self, user: SUserPut, method: str = None) -> Tuple[bool, Optional[User]]:
        user = User(**user.model_dump())
        if method == "withdraw":
            current_user = self.user_repo.get_user_by_id(user_id=user.user_id)
            if current_user.balance < abs(user.balance):
                return False, None
        return True, await self.user_repo.change_balance(user)
    

    async def get_user_by_id(self, user_id: int) -> User:
        return await self.user_repo.get_user_by_id(user_id=user_id)
    



