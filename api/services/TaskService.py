from api.repositories import TaskRepository
from api.schemas import STaskGet
from db import Task
from core.loader import bot
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Optional


class TaskService():
    def __init__(self, db_session: AsyncSession) -> None:
        self.task_repo = TaskRepository(db_session)

    async def get_task_by_id(self, task_id: int) -> Optional[Task]:
        return await self.task_repo.get_task_by_id(task_id)

