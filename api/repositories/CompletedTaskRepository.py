
from typing import Optional, Tuple

from sqlalchemy import select
from db import Task, CompletedTask
from sqlalchemy.ext.asyncio import AsyncSession

class CompletedTaskRepository:
    def __init__(self, session: AsyncSession):
        self.session = session        

    async def get_uncompleted_tasks_by_user_id(self, user_id: int) -> Optional[Tuple[Task]]:
        query = select(Task).outerjoin(CompletedTask, (Task.task_id == CompletedTask.task_id) & (CompletedTask.user_id == user_id)).filter(CompletedTask.task_id == None)
        res = await self.session.execute(query)
        return res.scalars().all()

    async def add_completed_task(self, completed_task: CompletedTask) -> Optional[CompletedTask]:
        self.session.add(completed_task)
        await self.session.commit()
        return completed_task
    
    async def get_completed_task_by_task_id(self, task_id: int) -> Optional[CompletedTask]:
        res = await self.session.execute(select(CompletedTask).where(CompletedTask.task_id == task_id))
        return res.scalar_one_or_none()





