from typing import Optional, Tuple

from sqlalchemy import select, update
from db import User
from sqlalchemy.ext.asyncio import AsyncSession

class UserRepository:
    def __init__(self, session: AsyncSession):
        self.session = session
    
    async def get_user_by_id(self, user_id: int) -> Optional[User]:
        res = await self.session.execute(select(User).filter(User.user_id == user_id))
        return res.scalar_one_or_none()
    
    async def get_referals_by_user_id(self, user_id: int) -> Optional[Tuple[User]]:
        res = await self.session.execute(select(User).filter(User.referal_id == user_id))
        return res.sclarals().all()
    
    async def add_user(self, user: User) -> User:
        self.session.add(user)
        await self.session.commit()
        return User
    
    async def change_balance(self, user: User) -> User:
        query = update(User).where(User.user_id == user.user_id).values(balance=User.balance + user.balance)
        await self.session.execute(query)
        await self.session.commit()
        updated_user = await self.session.execute(select(User).where(User.user_id == user.user_id))
        return updated_user.scalar_one()

