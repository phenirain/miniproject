from typing import Optional, Tuple

from sqlalchemy import select
from db import Task
from sqlalchemy.ext.asyncio import AsyncSession

class TaskRepository:
    def __init__(self, session: AsyncSession):
        self.session = session
    
    async def get_tasks(self) -> Optional[Tuple[Task]]:
        res = await self.session.execute(select(Task))
        return res.scalars.all()

    async def get_task_by_id(self, task_id: int) -> Optional[Task]:
        res = await self.session.execute(select(Task).filter(Task.task_id == task_id))
        return res.scalar_one_or_none()

