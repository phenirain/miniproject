from fastapi import APIRouter, Depends, HTTPException
from api.services import UserService, CompletedService, TaskService
from sqlalchemy.ext.asyncio import AsyncSession
from db import get_async_session
from api.schemas import SUser, SCompletedTask, SUserPut



v1_router = APIRouter(prefix='/v1')


async def get_user_service(db_session: AsyncSession = Depends(get_async_session)) -> UserService:
    user_service = UserService(db_session)
    return user_service

async def get_task_service(db_session: AsyncSession = Depends(get_async_session)) -> TaskService:
    task_service = TaskService(db_session)
    return task_service

async def get_completed_service(db_session: AsyncSession = Depends(get_async_session)) -> CompletedService:
    completed_service = CompletedService(db_session)
    return completed_service


@v1_router.post('/user')
async def add_user(user: SUser, user_service: UserService = Depends(get_user_service)):
    if await user_service.exist(user):
        raise HTTPException(409, "User already exists")
    user = await user_service.register_user(user)
    return user


@v1_router.get('/user/{user_id}')
async def get_user(user_id: int, user_service: UserService = Depends(get_user_service)):
    return await user_service.get_user_by_id(user_id)


@v1_router.get('/user/referals/{user_id}')
async def get_referals(user_id: int, ):
    pass


@v1_router.get('/tasks/{user_id}')
async def get_tasks(user_id: int, completed_service: CompletedService = Depends(get_completed_service)):
    return await completed_service.get_uncompleted_tasks_by_user_id(user_id)


@v1_router.post('/check')
async def get_check(completed_check: SCompletedTask, completed_service: CompletedService = Depends(get_completed_service)): 
    res, completed_task = await completed_service.check_task(completed_check)
    if res:
        return completed_task
    else:
        raise HTTPException(403, "Task isn`t already completed")
    

@v1_router.post('/deposit')
async def deposit(user: SUserPut, user_service: UserService = Depends(get_user_service)):
    res, current_user = await user_service.change_balance(user)
    if res:
        return current_user

@v1_router.post('/withdraw')
async def withdraw(user: SUserPut, user_service: UserService = Depends(get_user_service)):
    res, current_user = await user_service.change_balance(user, method="withdraw")
    if res:
        return current_user
    else:
        raise HTTPException(403, "Insufficient funds")