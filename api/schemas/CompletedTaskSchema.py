from typing import Optional
from datetime import datetime

from pydantic import BaseModel


class SCompletedTask(BaseModel):
    task_id: int
    user_id: int

    class Config:
        from_attributes = True





