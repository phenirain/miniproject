from typing import Optional
from datetime import datetime

from pydantic import BaseModel


class STaskAdd(BaseModel):
    title: str
    url: str
    award: float
    task_added_at: Optional[datetime] = None
    task_ended_at: Optional[datetime] = None

class STaskGet(STaskAdd):
    task_id: int

    class Config:
        from_attributes = True


