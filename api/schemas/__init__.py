from .TaskSchema import STaskAdd, STaskGet
from .UserSchema import SUser, SUserReferals, SUserPut
from .CompletedTaskSchema import SCompletedTask

