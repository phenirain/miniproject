from typing import Optional, Tuple
from pydantic import BaseModel, Field
from db import User



class SUser(BaseModel):
    user_id: int
    balance: float = Field(default=0.0, alias="balance")
    referal_id: Optional[int] = Field(alias="referal_id")

    class Config:
        from_attributes = True


class SUserReferals(BaseModel):
    user_id: int
    referals: Optional[Tuple[SUser]]


class SUserPut(BaseModel):
    user_id: int
    balance: float
